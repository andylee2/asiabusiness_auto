# _*_ coding: UTF-8 _*_
import time
import unittest
from HTMLTestRunner import HTMLTestRunner
from datetime import datetime
import os
from Games import Baccarat, DragonTiger, FanTan


class LiveReport(unittest.TestCase):
    """ RG真人API報表 """

    baccarat_ar = Baccarat.BaccaratAutoReport()
    dragontiger_ar = DragonTiger.DragonTigerAutoReport()
    fantan_ar = FanTan.FanTanAutoReport


    def test_baccarat_report(self):
        """ 亞洲商務_百家樂API報表確認 """

        result = self.baccarat_ar.check_odds()
        self.assertEqual(True, result)

    def test_dragontiger_report(self):
        """ 亞洲商務_龍虎API報表確認 """

        result = self.dragontiger_ar.check_odds()
        self.assertEqual(True, result)

    def test_fantan_report(self):
        """ 亞洲商務_百家樂API報表確認 """

        result = self.fantan_ar.check_odds()
        self.assertEqual(True, result)


if __name__ == '__main__':
    test_units = unittest.TestSuite()
    test_units.addTests([
        LiveReport("test_baccarat_report"),
        LiveReport("test_dragontiger_report"),
        LiveReport("test_fantan_report")
    ])

    now = datetime.now().strftime('%m-%d %H_%M_%S')
    filename = './Report/API/API_' + now + '.html'
    with open(filename, 'wb+') as fp:
        runner = HTMLTestRunner(
            stream=fp,
            verbosity=2,
            title='自動API報表',
        )
        runner.run(test_units)
