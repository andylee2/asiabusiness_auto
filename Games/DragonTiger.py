# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from Setting import *
from Lib.check_functions import CheckFunctions as cf
from Lib.check_report_api import CheckReportApi as ca

# ====== 開牌結果  ======
# 龍虎 R-B
# [ "1K", "3J" ] 龍
# [ "42", "24" ] 虎
# [ "26", "46" ] 和
# [ "2J", "14" ] 龍
# [ "34", "1A" ] 虎
# [ "45", "45" ] 和

game_round1 = ["bet-text red big"]                          # 龍
game_round2 = ["bet-text blue big"]                         # 虎
game_round3 = ["bet-text green big"]                        # 和
game_round4 = ["bet-text red big", "bet-text blue big"]     # 龍,虎(和局退一半)

# 賠率表
odds_list = [
    # 賠率ID: 數值, 注區名稱: 注區
    {"00001": 1, "AreaName": "龍"},
    {"00002": 1, "AreaName": "虎"},
    {"00003": 8, "AreaName": "和"},
]


class DragonTigerAutoBet:
    """ 亞洲商務_龍虎自動下注 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_bet(self):
        """ 自動下注 """

        # init
        all_game_round = {
            "game_round1": False,
            "game_round2": False,
            "game_round3": False,
            "game_round4": False,
        }
        bet_finish = False
        result_list = []
        count = 0
        total_count = 0
        c_f = cf(self.driver, self.get_img)

        try:
            # 點擊龍虎頁籤
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='game-menu']//span[text()='龙虎']"))).click()
            # 進R-B桌
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='R-B']/../../..//div[text()='进入游戏']"))).click()
            time.sleep(1)
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='R-B']/../../..//div[@class='limit']"))).click()

            # 判斷是否進桌
            WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(
                (By.XPATH, f"//div[text()='{test_data['member_account'][0]}']")))

        except Exception as e:
            self.get_img("Fail：龍虎 R-B 進桌失敗")
            print(e)
            return False

        try:
            while not bet_finish and total_count < 2:

                # 抓取當前局號
                bet_round = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@class='table-number']/div[@class='value']"))).text
                bet_round = (int(bet_round[-4:]) % 6)

                # 撈取倒數秒數
                bet_sec = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@class='timer-count']"))).text

                # 倒數秒數不為空 且大於 3 秒才進行下注
                if not bet_sec == "" and int(bet_sec) > 3:

                    # 當局餘數=1
                    if bet_round == 1:
                        print("下注注區：龍")
                        for i in game_round1:
                            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//div[@class='{i}']"))).click()
                        check_result = c_f.check_bet_success("game_round1", all_game_round)
                        if check_result:
                            all_game_round = check_result

                    # 當局餘數=2
                    elif bet_round == 2:
                        print("下注注區：虎")
                        for i in game_round2:
                            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//div[@class='{i}']"))).click()
                        check_result = c_f.check_bet_success("game_round2", all_game_round)
                        if check_result:
                            all_game_round = check_result

                    # 當局餘數=3
                    elif bet_round == 3:
                        print("下注注區：和")
                        for i in game_round3:
                            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//div[@class='{i}']"))).click()
                        check_result = c_f.check_bet_success("game_round3", all_game_round)
                        if check_result:
                            all_game_round = check_result

                    # 當局餘數=0
                    elif bet_round == 0:
                        print("(和局退一半)下注注區：龍、虎")
                        for i in game_round4:
                            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//div[@class='{i}']"))).click()
                        check_result = c_f.check_bet_success("game_round4", all_game_round)
                        if check_result:
                            all_game_round = check_result

                    # 等該局結束
                    WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[@data-i18n='stopbetting']")))

                    # 等待新局出現
                    WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[@data-i18n='startbetting']")))
                    time.sleep(2)

                else:
                    # 等該局結束
                    WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[@data-i18n='stopbetting']")))

                    # 等待新局出現
                    WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[@data-i18n='startbetting']")))
                    time.sleep(2)

                # 計算下注次數
                count += 1
                if count % 4 == 0:
                    total_count += 1

                # 判斷是否皆下注完成
                bet_finish = c_f.check_all_round(all_game_round)

            # 結束迴圈後判斷是否都成功
            bet_finish = c_f.check_all_round(all_game_round)
            if not bet_finish:
                result_list.append("False")

            # 返回大廳
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()=' 返回大厅 ']"))).click()

        except Exception as e:
            self.get_img("Fail:「龍虎」自動下注失敗")
            print(e)
            result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results


class DragonTigerAutoReport:
    """ 亞洲商務_龍虎自動API報表 """

    @staticmethod
    def check_odds():
        """ 確認賠率及派彩 """

        result_list = []
        response_data = ca().get_report(game_name='LongHu')
        if not response_data:
            return False

        # 確認所有賠率
        check_result = ca().check_all_odds(response_data, odds_list)
        if not check_result:
            result_list.append("False")

        # 進行相關比對
        for bet_data in response_data:
            # 和局退一半，另外計算 (賠率值顯示1)
            if int(bet_data['NoActive']) % 6 == 0:
                win_lose = bet_data['BetPoint'] + bet_data['WinLosePoint']
                if win_lose != bet_data['BetPoint'] / 2:
                    print("\nFail：和局時，退回一半下注金額，注單派彩錯誤")
                    print(f"注單編號：{bet_data['RecordId']}")
                    print(f"賠率ID：{bet_data['AreaId']}")
                    print(f"賠率值：{bet_data['Odds']}")
                    print(f"注區名稱：{bet_data['AreaName']}")
                    print(f"下注金額：{bet_data['BetPoint']}")
                    print(f"派彩金額：{bet_data['WinLosePoint']}")
                    print(f"下注 + 派彩金額：{win_lose}")
                    result_list.append("False")

            else:
                # 判斷賠率是否正確
                odds_result = ca().check_odds(bet_data, odds_list)
                if odds_result:
                    # 判斷派彩是否為正
                    win_lose_result = ca().check_win_lose(bet_data)
                    if win_lose_result:
                        # 判斷派彩計算是否正確
                        pay_amount_result = ca().check_pay_amount(bet_data)
                        if pay_amount_result:
                            pass
                        else:
                            result_list.append("False")
                    else:
                        result_list.append("False")
                else:
                    result_list.append("False")

        results = False if "False" in str(result_list) else True
        if results:
            print(f"注單數量：{len(response_data)} 全部正常")

        return results
