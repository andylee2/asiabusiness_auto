# _*_ coding: UTF-8 _*_
import traceback

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
from Setting import *
from Lib.check_functions import CheckFunctions as cf
from Lib.check_report_api import CheckReportApi as ca

# [ 1 ] (贏)1正、1番、1念2、1念3、1念4、12角、14角、123門、2通13、2通14、3通12、3通14、4通12、4通13、單  (和)2念1、3念1、4念1、2通34、3通24、4通23、2正、4正
# [ 2 ] (贏)2正、2番、2念1、2念3、2念4、124門、1通23、1通24、3通24、4通23、雙  (和)1念2、3念2、4念2、1通34、3通14、4通13、1正、3正
# [ 3 ] (贏)3正、3番、3念1、3念2、3念4、23角、34角、134門、1通34、2通34  (和)1念3、2念3、4念3、1通24、2通14、4通12
# [ 4 ] (贏)4正、4番、4念1、4念2、4念3、234門                   (和)1念4、2念4、3念4、1通23、2通13、3通12

game_round1 = ["1正", "1番", "1念2", "1念3", "1念4", "12角", "14角", "123 门", "2通13", "2通14", "3通12", "3通14", "4通12", "4通13", "单"]
game_round2 = ["2正", "2番", "2念1", "2念3", "2念4", "124 门", "1通23", "1通24", "3通24", "4通23", "双"]
game_round3 = ["3正", "3番", "3念1", "3念2", "3念4", "23角", "34角", "134 门", "1通34", "2通34"]
game_round4 = ["4正", "4番", "4念1", "4念2", "4念3", "234 门"]
game_round5 = ["2念1", "3念1", "4念1", "2通34", "3通24", "4通23", "2正", "4正"]
game_round6 = ["1念2", "3念2", "4念2", "1通34", "3通14", "4通13", "1正", "3正"]
game_round7 = ["1念3", "2念3", "4念3", "1通24", "2通14", "4通12"]
game_round8 = ["1念4", "2念4", "3念4", "1通23", "2通13", "3通12"]

# 賠率表
odds_list = [
    # 賠率ID: 數值, 注區名稱: 注區
    {"00027": 0.95, "AreaName": "1正"},
    {"00028": 0.95, "AreaName": "2正"},
    {"00029": 0.95, "AreaName": "3正"},
    {"00030": 0.95, "AreaName": "4正"},
    {"00031": 1.9, "AreaName": "1念2"},
    {"00032": 1.9, "AreaName": "1念3"},
    {"00033": 1.9, "AreaName": "1念4"},
    {"00034": 1.9, "AreaName": "2念1"},
    {"00035": 1.9, "AreaName": "2念3"},
    {"00036": 1.9, "AreaName": "2念4"},
    {"00037": 1.9, "AreaName": "3念1"},
    {"00038": 1.9, "AreaName": "3念2"},
    {"00039": 1.9, "AreaName": "3念4"},
    {"00040": 1.9, "AreaName": "4念1"},
    {"00041": 1.9, "AreaName": "4念2"},
    {"00042": 1.9, "AreaName": "4念3"},
    {"00043": 0.475, "AreaName": "1通23"},
    {"00044": 0.475, "AreaName": "1通24"},
    {"00045": 0.475, "AreaName": "1通34"},
    {"00046": 0.475, "AreaName": "2通13"},
    {"00047": 0.475, "AreaName": "2通14"},
    {"00048": 0.475, "AreaName": "2通34"},
    {"00049": 0.475, "AreaName": "3通12"},
    {"00050": 0.475, "AreaName": "3通14"},
    {"00051": 0.475, "AreaName": "3通24"},
    {"00052": 0.475, "AreaName": "4通12"},
    {"00053": 0.475, "AreaName": "4通13"},
    {"00054": 0.475, "AreaName": "4通23"},
    {"00055": 0.95, "AreaName": "單"},
    {"00056": 0.95, "AreaName": "雙"},
    {"00057": 0.3167, "AreaName": "123門"},
    {"00058": 0.3167, "AreaName": "124門"},
    {"00059": 0.3167, "AreaName": "134門"},
    {"00060": 0.3167, "AreaName": "234門"},
    {"00061": 2.85, "AreaName": "1番"},
    {"00062": 2.85, "AreaName": "2番"},
    {"00063": 2.85, "AreaName": "3番"},
    {"00064": 2.85, "AreaName": "4番"},
    {"00065": 0.95, "AreaName": "12角"},
    {"00067": 0.95, "AreaName": "14角"},
    {"00068": 0.95, "AreaName": "23角"},
    {"00070": 0.95, "AreaName": "34角"},
]


class FanTanAutoBet:
    """ 亞洲商務_番攤自動下注 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_bet(self):
        """ 自動下注 """

        # init
        all_game_round = {
            "game_round1": False,
            "game_round2": False,
            "game_round3": False,
            "game_round4": False,
            "game_round5": False,
            "game_round6": False,
            "game_round7": False,
            "game_round8": False,
        }
        bet_finish = False
        result_list = []
        count = 0
        total_count = 0
        c_f = cf(self.driver, self.get_img)
        ac = ActionChains(self.driver)

        try:
            # 點擊番攤頁籤
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='game-menu']//span[text()='番摊']"))).click()
            # 進R-B桌
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='R-B']/../../..//div[text()='进入游戏']"))).click()
            time.sleep(1)
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='R-B']/../../..//div[@class='limit']"))).click()

            # 判斷是否進桌
            WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(
                (By.XPATH, f"//div[text()='{test_data['member_account'][0]}']")))

            WebDriverWait(self.driver, 10).until_not(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='loading-container']")))

        except Exception as e:
            self.get_img("Fail：進入番攤R-B失敗")
            print(e)
            return False

        try:
            while not bet_finish and total_count < 2:

                # 等待新局出現
                WebDriverWait(self.driver, 70).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[text()='请下注']")))

                # 抓取當前局號
                bet_round = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@data-i18n='serialnumber']"))).text
                bet_round = (int(bet_round[-4:]) % 8)

                # 撈取倒數秒數
                bet_sec = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@class='count']"))).text

                # 當倒數秒數不為空且大於5秒時才下注
                if not bet_sec == "" and int(bet_sec) > 15:

                    # 當局餘數=1
                    if bet_round == 1:
                        print("下注注區：(贏)1正、1番、1念2、1念3、1念4、12角、14角、123門、2通13、2通14、3通12、3通14、4通12、4通13、單")
                        for i in game_round1:
                            _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//div[text()='{i}']")))
                            ac.move_to_element(_ele).click().perform()
                        check_result = c_f.check_bet_success("game_round1", all_game_round, fantan=True)
                        if check_result:
                            all_game_round = check_result

                    # 當局餘數=2
                    elif bet_round == 2:
                        print("下注注區：(贏)2正、2番、2念1、2念3、2念4、124門、1通23、1通24、3通24、4通23、雙")
                        for i in game_round2:
                            _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//div[text()='{i}']")))
                            ac.move_to_element(_ele).click().perform()
                        check_result = c_f.check_bet_success("game_round2", all_game_round, fantan=True)
                        if check_result:
                            all_game_round = check_result

                    # 當局餘數=3
                    elif bet_round == 3:
                        print("下注注區：(贏)3正、3番、3念1、3念2、3念4、23角、34角、134門、1通34、2通34")
                        for i in game_round3:
                            _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//div[text()='{i}']")))
                            ac.move_to_element(_ele).click().perform()
                        check_result = c_f.check_bet_success("game_round3", all_game_round, fantan=True)
                        if check_result:
                            all_game_round = check_result

                    # 當局餘數=4
                    elif bet_round == 4:
                        print("下注注區：(贏)4正、4番、4念1、4念2、4念3、234門")
                        for i in game_round4:
                            _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//div[text()='{i}']")))
                            ac.move_to_element(_ele).click().perform()
                        check_result = c_f.check_bet_success("game_round4", all_game_round, fantan=True)
                        if check_result:
                            all_game_round = check_result

                    # 當局餘數=5
                    elif bet_round == 5:
                        print("下注注區：(和局退錢)2念1、3念1、4念1、2通34、3通24、4通23、2正、4正")
                        for i in game_round5:
                            _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//div[text()='{i}']")))
                            ac.move_to_element(_ele).click().perform()
                        check_result = c_f.check_bet_success("game_round5", all_game_round, fantan=True)
                        if check_result:
                            all_game_round = check_result

                    # 當局餘數=6
                    elif bet_round == 6:
                        print("下注注區：(和局退錢)1念2、3念2、4念2、1通34、3通14、4通13、1正、3正")
                        for i in game_round6:
                            _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//div[text()='{i}']")))
                            ac.move_to_element(_ele).click().perform()
                        check_result = c_f.check_bet_success("game_round6", all_game_round, fantan=True)
                        if check_result:
                            all_game_round = check_result

                    # 當局餘數=7
                    elif bet_round == 7:
                        print("下注注區：(和局退錢)1念3、2念3、4念3、1通24、2通14、4通12")
                        for i in game_round7:
                            _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//div[text()='{i}']")))
                            ac.move_to_element(_ele).click().perform()
                        check_result = c_f.check_bet_success("game_round7", all_game_round, fantan=True)
                        if check_result:
                            all_game_round = check_result

                    # 當局餘數=0
                    elif bet_round == 0:
                        print("下注注區：(和局退錢)1念4、2念4、3念4、1通23、2通13、3通12")
                        for i in game_round8:
                            _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//div[text()='{i}']")))
                            ac.move_to_element(_ele).click().perform()
                        check_result = c_f.check_bet_success("game_round8", all_game_round, fantan=True)
                        if check_result:
                            all_game_round = check_result

                    # 等該局結束
                    WebDriverWait(self.driver, 70).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[text()='结算中']")))


                else:
                    # 等該局結束
                    WebDriverWait(self.driver, 70).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[text()='结算中']")))

                # 計算下注次數
                count += 1
                if count % 8 == 0:
                    total_count += 1

                # 判斷是否皆下注完成
                bet_finish = c_f.check_all_round(all_game_round)

            # 結束迴圈後判斷是否都成功
            bet_finish = c_f.check_all_round(all_game_round)
            if not bet_finish:
                result_list.append("False")

            # 返回大廳
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='back-button']"))).click()

        except Exception as e:
            self.get_img("Fail:「番攤」自動下注失敗")
            print(e)
            result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results


class FanTanAutoReport:
    """ 亞洲商務_番攤自動API報表 """

    @staticmethod
    def check_odds():
        """ 確認賠率及派彩 """

        result_list = []
        response_data = ca().get_report(game_name='FanTan')
        if not response_data:
            return False

        # 確認所有賠率
        check_result = ca().check_all_odds(response_data, odds_list)
        if not check_result:
            result_list.append("False")

        # 進行相關比對
        for bet_data in response_data:
            # 抓取局號
            bet_round = int(bet_data['NoActive']) % 8

            # 判斷賠率是否正確
            odds_result = ca().check_odds(bet_data, odds_list)
            if odds_result:
                # 判斷派彩是否為正
                win_lose_result = ca().check_win_lose(bet_data)
                if win_lose_result:
                    # 判斷派彩計算是否正確(若局號餘數為6或0，驗證和局退錢)
                    if bet_round == 5 or bet_round == 6 or bet_round == 7 or bet_round == 0:
                        if bet_data['WinLosePoint'] == 0:
                            pass
                        else:
                            print("\nFail：和局派彩有誤")
                            print(f"注單編號：{bet_data['RecordId']}")
                            print(f"賠率ID：{bet_data['AreaId']}")
                            print(f"下注金額：{bet_data['BetPoint']}")
                            print(f"派彩：{bet_data['WinLosePoint']}")
                            print(int(bet_data['NoActive']))
                            result_list.append("False")
                    else:
                        pay_amount_result = ca().check_pay_amount(bet_data)
                        if pay_amount_result:
                            pass
                        else:
                            result_list.append("False")
                else:
                    result_list.append("False")
            else:
                result_list.append("False")

        results = False if "False" in str(result_list) else True
        if results:
            print(f"注單數量：{len(response_data)} 全部正常")

        return results
