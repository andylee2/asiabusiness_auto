# _*_ coding: UTF-8 _*_
import time
import decimal
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from Setting import *


def number_check(number):
    """
    確認數字
    參數:
        number (str|float): 數字

    Returns:
        (float): 判斷最後一位是否為 0，為 0 回傳當前數字，不為 0 捨棄最後一位
    """

    try:
        number = str(number)
        if '.' in number:
            last_number = number.split('.')[1]
            if len(last_number) >= 2:
                if last_number[-1] == '0':
                    return float(number)
                else:
                    return float(number[:-1])
            else:
                return float(number)
        else:
            return float(number)

    except Exception as e:
        print("Fail：確認數字失敗")
        print(e)
        return float(0)


def check_member_report_and_manager_report(member_report, manager_report):
    """
    確認官網報表資料與後台報表資料
    參數:
        member_report (list): 官網的報表注單資料
        manager_report (dict): 後台or超帳的報表注單資料

    Returns:
        (bool): 比對資料，成功為True，失敗or異常則是False
    """

    try:
        if not member_report or not manager_report:
            print("Fail：報表資料有誤")
            print(f"官網報表：{member_report}")
            print(f"後台報表：{manager_report}\n")
            return False

        # init
        result_list = []

        print("==================== 官網報表與後台報表 - 檢查開始 ====================\n")
        print("檢查項目：「官網報表」與「後台報表」比對")

        for member_bet in member_report:
            for manager_bet in manager_report:
                if member_bet['编号'] == manager_bet['编号']:
                    member_key = list(member_bet['内容'].keys())[0]
                    manager_key = list(manager_bet['内容'].keys())[0]

                    # ======= 比對第一層 =======
                    # 比對下注金額
                    if member_bet['下注金额'] != manager_bet['下注金额']:
                        print("Fail：官網報表「下注金額」與後台會員報表「下注金額」不同")
                        print(f"編號：{member_bet['编号']}")
                        print(f"官網報表「總下注金額」：{member_bet['下注金额']}")
                        print(f"後台會員報表「總下注金額」：{manager_bet['下注金额']}\n")
                        result_list.append("False")

                    # 比對實投量
                    elif member_bet['实投量'] != manager_bet['实投量']:
                        print("Fail：官網報表「實投量」與後台會員報表「實投量」不同")
                        print(f"編號：{member_bet['编号']}")
                        print(f"官網報表「實投量」：{member_bet['实投量']}")
                        print(f"後台會員報表「實投量」：{manager_bet['实投量']}\n")
                        result_list.append("False")

                    # 比對會員結果
                    elif member_bet['会员结果'] != manager_bet['会员结果']:
                        print("Fail：官網報表「會員結果」與後台會員報表「會員結果」不同")
                        print(f"編號：{member_bet['编号']}")
                        print(f"官網報表「會員結果」：{member_bet['会员结果']}")
                        print(f"後台會員報表「會員結果」：{manager_bet['会员结果']}\n")
                        result_list.append("False")

                    # ======= 比對第二層 =======
                    for member_second_bet in member_bet['内容'][member_key]:
                        for manager_second_bet in manager_bet['内容'][manager_key]:
                            if member_second_bet['交易单号'] == manager_second_bet['交易单号']:
                                # 比對下注金額
                                if member_second_bet['下注金额'] != manager_second_bet['下注金额']:
                                    print("Fail：官網報表「下注金額」與後台會員報表「下注金額」不同")
                                    print(f"編號：{member_bet['编号']}")
                                    print(f"交易單號：{member_second_bet['交易单号']}")
                                    print(f"官網報表「下注金額」：{member_second_bet['下注金额']}")
                                    print(f"後台會員報表「下注金額」：{manager_second_bet['下注金额']}\n")
                                    result_list.append("False")

                                num1, num2 = manager_second_bet['输赢金额'].split('.')
                                if len(num2) == 1:
                                    num2 = num2.ljust(2, '0')
                                manager_second_bet['输赢金额'] = num1 + '.' + num2

                                # 比對輸贏金額
                                if member_second_bet['输赢金额'] != manager_second_bet['输赢金额']:
                                    print("Fail：官網報表「輸贏金額」與後台會員報表「輸贏金額」不同")
                                    print(f"編號：{member_bet['编号']}")
                                    print(f"交易單號：{member_second_bet['交易单号']}")
                                    print(f"官網報表「輸贏金額」：{member_second_bet['输赢金额']}")
                                    print(f"後台會員報表「輸贏金額」：{manager_second_bet['输赢金额']}\n")
                                    result_list.append("False")

        print("*** 檢查結束 ***\n")
        print("==================== 官網報表與後台報表 - 檢查結束 ====================\n")

        results = False if "False" in str(result_list) else True
        return results

    except Exception as e:
        print("Fail：官網報表資料與後台報表比對異常")
        print(e)
        return False

class MemberReport:
    """ 官網報表 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def format_report(self):
        """
        整理注單報表

        Returns:
            (list|bool): 回傳整理後的注單報表，失敗or異常則是 False
        """

        try:
            # init
            bet_list = []
            field_dict = {}

            # 資料欄位
            th_list = self.driver.find_elements(By.XPATH, "//thead//th")
            for index, ele in enumerate(th_list):
                # +1 是給Xpath用
                field_dict[ele.text] = index + 1

            _ele = self.driver.find_elements(By.XPATH, "//tbody/tr")
            for tr in _ele:
                bet_data = {}
                for i in field_dict:
                    if i == '内容':
                        ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]")
                        bet_data[i] = {}
                        # 整理第二層報表
                        ele.click()
                        # wait loading
                        WebDriverWait(self.driver, 10).until_not(EC.presence_of_element_located(
                            (By.XPATH, "//div[@class='loading-container']")))
                        time.sleep(1)
                        bet_data[i][ele.text] = self.format_second_report()
                    else:
                        ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]")
                        bet_data[i] = ele.text.replace(',', '')
                bet_list.append(bet_data)

            self.get_img("亞洲商務客端報表")
            time.sleep(1)

            return bet_list

        except Exception as e:
            self.get_img("Fail：整理報表資料異常")
            print(e)
            return False

    def format_second_report(self):
        """
        整理注單第二層報表

        Returns:
            (list|bool): 回傳整理後的注單報表，失敗則是 False
        """

        try:
            # init
            second_bet_list = []
            second_field_dict = {}

            # 資料欄位
            second_th_list = self.driver.find_elements(By.XPATH, "//div[@class='rcg-detail-wrap']//thead//th")
            for index, ele in enumerate(second_th_list):
                # +1 是給Xpath用
                second_field_dict[ele.text] = index + 1

            _ele = self.driver.find_elements(By.XPATH, "//div[@class='rcg-detail-wrap']//tbody/tr")
            for tr in _ele:
                second_bet_data = {}
                for i in second_field_dict:
                    ele = tr.find_element(By.XPATH, f"./td[{second_field_dict[i]}]")
                    second_bet_data[i] = ele.text.replace(',', '')

                second_bet_list.append(second_bet_data)

            # 關閉第二層報表
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='rcg-detail-wrap']//div[@class='close-btn']"))).click()

            return second_bet_list

        except Exception as e:
            self.get_img("Fail：整理第二層報表異常")
            print(e)
            return False

    def check_web_and_report_data(self, bet_list):
        """
        確認官網統計資料與報表資料
        參數:
            bet_list (list): 報表注單資料

        Returns:
            (dict|bool): 回傳確認資料，異常則是False
        """

        try:
            if not bet_list:
                return False

            # init
            result_list = []
            check_data = {}

            # 統計比對數字
            # 報表總筆數
            report_total_number = len(bet_list)

            # 報表總下注金額(計算後再轉float避免顯示有誤)
            report_total_bet_amount = 0
            report_member_result = 0
            report_total_actual_bet_amount = 0
            report_total_win = 0
            report_total_rebate = 0
            for i in bet_list:
                report_total_bet_amount += decimal.Decimal(i['下注金额'])
                report_member_result += decimal.Decimal(i['会员结果'])
                report_total_actual_bet_amount += decimal.Decimal(i['实投量'])
                report_total_win += decimal.Decimal(i['输赢'])
                report_total_rebate += decimal.Decimal(i['退水'])
            report_total_bet_amount = float(report_total_bet_amount)
            report_member_result = float(report_member_result)
            report_total_actual_bet_amount = float(report_total_actual_bet_amount)
            report_total_win = float(report_total_win)
            report_total_rebate = float(report_total_rebate)

            # 取得報表最上方總數資料
            _ele = self.driver.find_elements(By.XPATH, "//div[@class='total-wrap']/span")
            for i in _ele:
                if "总投注" in i.text:
                    total_bet_amount = float("".join(filter(lambda s: s in '-0123456789.', i.text)))
                elif "总输赢" in i.text:
                    total_win = float("".join(filter(lambda s: s in '-0123456789.', i.text)))
                elif "总实投量" in i.text:
                    total_actual_bet_amount = float("".join(filter(lambda s: s in '-0123456789.', i.text)))
                elif "退水总计" in i.text:
                    total_rebate = float("".join(filter(lambda s: s in '-0123456789.', i.text)))
                elif "会员结果总计" in i.text:
                    total_member_result = float("".join(filter(lambda s: s in '-0123456789.', i.text)))
                elif "笔" in i.text:
                    total_number = int("".join(filter(lambda s: s in '-0123456789.', i.text)))

            print("==================== 官網報表 - 檢查開始 ====================\n")

            # 官網總筆數
            print("檢查項目：官網「總筆數」與報表「總筆數」比對")
            if total_number != report_total_number:
                print("Fail：官網「總筆數」與報表「總筆數」不同")
                print(f"官網「總筆數」：{total_number}")
                print(f"報表「總筆數」：{report_total_number}\n")
                result_list.append("False")
            else:
                check_data['總筆數'] = report_total_number
            print("*** 檢查結束 ***\n")

            # 總下注金額
            print("檢查項目：官網「總下注金額」與報表「總下注金額」比對")
            if total_bet_amount != report_total_bet_amount:
                print("Fail：官網「總下注金額」與報表「總下注金額」不同")
                print(f"官網「總下注金額」：{total_bet_amount}")
                print(f"報表「總下注金額」：{report_total_bet_amount}\n")
                result_list.append("False")
            else:
                check_data['總下注金額'] = report_total_bet_amount
            print("*** 檢查結束 ***\n")

            # 總會員結果
            print("檢查項目：官網「總會員結果」與報表「總會員結果」比對")
            if total_member_result != report_member_result:
                print("PS: 請依照「RG真人報表加總問題確認」文件進行確認")
                print("Fail：官網「總下注結果」與報表「總會員結果」不同")
                print(f"官網「總會員結果」：{total_member_result}")
                print(f"報表「總會員結果」：{report_member_result}\n")
                result_list.append("False")
            else:
                check_data['總會員結果'] = report_member_result
            print("*** 檢查結束 ***\n")

            # 總輸贏
            print("檢查項目：官網「總輸贏」與報表「總輸贏」比對")
            if total_win != report_total_win:
                print("Fail：官網「總輸贏」與報表「總輸贏」不同")
                print(f"官網「總輸贏」：{total_win}")
                print(f"報表「總輸贏」：{report_total_win}\n")
                result_list.append("False")
            else:
                check_data['總輸贏'] = report_total_win
            print("*** 檢查結束 ***\n")

            # 退水總計
            print("檢查項目：官網「退水總計」與報表「退水總計」比對")
            if total_rebate != report_total_rebate:
                print("Fail：官網「退水總計」與報表「退水總計」不同")
                print(f"官網「退水總計」：{total_rebate}")
                print(f"報表「退水總計」：{report_total_rebate}\n")
                result_list.append("False")
            else:
                check_data['退水總計'] = report_total_rebate
            print("*** 檢查結束 ***\n")

            # 總實投量
            print("檢查項目：官網「總實投量」與報表「總實投量」比對")
            if total_actual_bet_amount != report_total_actual_bet_amount:
                print("Fail：官網「總實投量」與報表「總實投量」不同")
                print(f"官網「總實投量」：{total_actual_bet_amount}")
                print(f"報表「總實投量」：{report_total_actual_bet_amount}\n")
                result_list.append("False")
            else:
                check_data['總實投量'] = report_total_actual_bet_amount
            print("*** 檢查結束 ***\n")

            print("檢查項目：「會員報表第一層」與「會員報表第二層」比對")
            for bet in bet_list:
                _name = list(bet['内容'].keys())[0]

                # 確認第一層與第二層金額、輸贏是否相同
                first_bet = decimal.Decimal(bet['下注金额'])
                first_win_lost = decimal.Decimal(bet['输赢'])
                second_bet = 0
                second_win_lost = 0

                # 計算第二層下注金額、輸贏金額
                for bet_second in bet['内容'][_name]:
                    second_bet += decimal.Decimal(bet_second['下注金额'])
                    second_win_lost += decimal.Decimal(bet_second['输赢金额'])

                # 計算後在轉float避免顯示有誤
                first_bet = float(first_bet)
                first_win_lost = float(first_win_lost)
                second_bet = float(second_bet)
                second_win_lost = float(second_win_lost)

                # 比對下注金額
                if first_bet != second_bet:
                    print("Fail：第一層「下注金額」與第二層不同")
                    print(f"編號：{bet['编号']}")
                    print(f"第一層「下注金額」：{first_bet}")
                    print(f"第二層「下注金額」：{second_bet}\n")
                    result_list.append("False")

                # 比對輸贏金額(因實際後台小數位問題，允許誤差值1以內)
                elif (first_win_lost - second_win_lost) > 1:
                    print("Fail：第一層「輸贏金額」與第二層不同")
                    print(f"編號：{bet['编号']}")
                    print(f"第一層「輸贏金額」：{first_win_lost}")
                    print(f"第二層「輸贏金額」：{second_win_lost}\n")
                    result_list.append("False")

            print("*** 檢查結束 ***\n")
            print("==================== 官網報表 - 檢查結束 ====================\n")

            check_data['確認結果'] = False if "False" in str(result_list) else True
            return check_data

        except Exception as e:
            self.get_img("Fail：確認官網統計資料與報表資料異常")
            print(e)
            return False


class ManageReport:
    """ 後端報表 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def search_report(self, check_data=''):
        """
        搜尋報表
        參數:
            check_data (dict): 官網的確認資料，預設為空

        Returns:
            Tuple[list, dict, bool|bool, bool, bool]: 查詢報表成功，回傳注單報表, 頁面統計資料, 頁面檢查成功，失敗or無注單則是 False
        """

        # init
        field_dict = {}
        result = True

        # 判斷是否有資料
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, f"//td[text()='{test_data['member_account'][0]}']")))
            time.sleep(1)

            # 資料欄位
            th_list = self.driver.find_elements(By.XPATH, "//thead//th")

            # 欄位清單
            search_list = ['笔数', '下注金额', '实投量', '会员结果']

            for index, ele in enumerate(th_list):
                # 取特定欄位
                if ele.text in search_list:
                    # +1 是給Xpath用
                    field_dict[ele.text] = index + 1

            page_data, page_check_result = self.check_search_page(field_dict, check_data)
            if not page_check_result:
                result = False

            # 點擊下注金額來進入報表
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, f"//td[text()='{test_data['member_account'][0]}']/../td[{field_dict['下注金额']}]/span"))).click()
            time.sleep(1)
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, f"//font[text()='会员:{test_data['member_account'][0]}']")))

            # 切換頁面顯示100筆資料
            select = Select(self.driver.find_element(By.XPATH, "//select[@name='oTable_c_r_detail_length']"))
            select.select_by_value("100")
            time.sleep(2)

            report_data = self.format_report()

            return report_data, page_data, result

        except Exception as e:
            self.get_img(f"Fail：帳號-{test_data['member_account'][0]} 查無資料")
            print(e)
            return False, False, False

    def check_search_page(self, field_dict, check_data):
        """
        確認搜尋結果頁面統計資料
        參數:
            field_dict (dict): 欄位資料
            check_data (dict): 官網的確認資料

        Returns:
             Tuple[dict|dict, bool|bool, bool]:  確認搜尋結果頁面統計資料，比對成功回傳頁面統計資料，失敗or異常則是 False
        """

        try:
            # init
            page_data = {}
            result_list = []

            print(f"==================== 後台報表 - 檢查開始 ====================\n")

            # 取得搜尋頁面資料
            for i in field_dict:
                _ele = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                    (By.XPATH, f"//tr[./td[text()='{test_data['member_account'][0]}']]/td[{field_dict[i]}]")))
                if i == '笔数':
                    page_data[i] = int(_ele.text)
                else:
                    page_data[i] = float(_ele.text.replace(',', ''))

            # 檢查check_data
            if check_data:
                check_result = all(key in check_data for key in ['總筆數', '總下注金額', '總會員結果', '總實投量'])
            else:
                check_result = False

            # 有資料才進行比對
            if check_result:
                print("檢查項目：「官網總計」與「後台總計」比對")

                # 資料處裡
                total_bet = number_check(check_data['總下注金額'])
                total_win_lose = number_check(check_data['總會員結果'])
                total_actual_bet = number_check(check_data['總實投量'])

                # 比對官網資料
                if page_data['笔数'] != check_data['總筆數']:
                    print("Fail：官網「總筆數」與後台不同")
                    print(f"官網「總筆數」：{check_data['總筆數']}")
                    print(f"後台「總筆數」：{page_data['笔数']}\n")
                    result_list.append("False")

                elif page_data['下注金额'] != total_bet:
                    print("Fail：官網「總下注金額」與後台不同")
                    print(f"官網「總下注金額」：{total_bet}")
                    print(f"後台「總下注金額」：{page_data['下注金额']}\n")
                    result_list.append("False")

                elif page_data['实投量'] != total_actual_bet:
                    print("Fail：官網「總實投量」與後台不同")
                    print(f"官網「總實投量」：{total_actual_bet}")
                    print(f"後台「總實投量」：{page_data['实投量']}\n")
                    result_list.append("False")

                elif page_data['会员结果'] != total_win_lose:
                    print(f"Fail：官網「會員輸贏」與後台不同")
                    print(f"官網「會員輸贏」：{total_win_lose}")
                    print(f"後台「會員輸贏」：{page_data['会员结果']}\n")
                    result_list.append("False")

                print("*** 檢查結束 ***\n")

                self.get_img("後台搜尋結果頁面")
                time.sleep(1)

                results = False if "False" in str(result_list) else True
                return page_data, results

            else:
                return page_data, True

        except Exception as e:
            self.get_img("Fail：確認搜尋結果頁面資料異常")
            print(e)
            return False, False

    def format_report(self):
        """
        整理注單報表

        Returns:
            (list|bool): 回傳整理後的注單報表，失敗or異常則是 False
        """

        try:
            # init
            bet_list = []
            field_dict = {}

            # 資料欄位
            th_list = self.driver.find_elements(By.XPATH, "//thead//th")
            for index, ele in enumerate(th_list):
                # +1 是給Xpath用
                field_dict[ele.text] = index + 1

            _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_all_elements_located(
                (By.XPATH, f"//td[text()='{test_data['member_account'][0]}']/..")))

            for tr in _ele:
                bet_data = {}
                for i in field_dict:
                    # 時間要處裡換行
                    if i == '时间':
                        ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]")
                        _number, _time = ele.text.split('\n')
                        bet_data['编号'] = _number
                        bet_data[i] = _time
                    elif i == '内容':
                        ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]/span")
                        bet_data[i] = {}
                        # 整理第二層報表
                        ele.click()
                        time.sleep(1)
                        bet_data[i][ele.text] = self.format_second_report()
                    elif i == '会员输赢':
                        ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]")
                        bet_data[i] = format(float(ele.text.replace(',', '')), '.2f')
                    else:
                        ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]")
                        bet_data[i] = ele.text.replace(',', '')

                bet_list.append(bet_data)

            self.get_img("後台報表")
            time.sleep(1)

            return bet_list

        except Exception as e:
            print(e)
            self.get_img("Fail：整理報表資料異常")
            return False

    def format_second_report(self):
        """
        整理注單第二層報表

        Returns:
            (list|bool): 回傳整理後的注單報表，失敗則是 False
        """

        try:
            # init
            second_bet_list = []
            second_field_dict = {}

            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//h5[text()='开牌日志']")))

            # 切換頁面顯示100筆資料
            select = Select(self.driver.find_element(By.XPATH, "//select[@name='oTable_c_o_t_EGame2_length']"))
            select.select_by_value("100")
            time.sleep(2)

            # 資料欄位
            second_th_list = self.driver.find_elements(By.XPATH, "//th[text()='交易单号']/../th")
            for index, ele in enumerate(second_th_list):
                # +1 是給Xpath用
                second_field_dict[ele.text] = index + 1

            _ele = self.driver.find_elements(By.XPATH, "//th[text()='交易单号']/../../following-sibling::tbody/tr")

            for tr in _ele:
                second_bet_data = {}
                for i in second_field_dict:
                    ele = tr.find_element(By.XPATH, f"./td[{second_field_dict[i]}]")
                    second_bet_data[i] = ele.text.replace(',', '')

                second_bet_list.append(second_bet_data)

            # 關閉第二層報表
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//button[@aria-label='Close']"))).click()

            return second_bet_list

        except:
            self.get_img("Fail：整理第二層報表異常")
            return False

    def check_web_and_report_data(self, bet_list, page_data):
        """
        確認後台統計資料與報表資料
        參數:
            bet_list (list): 後台報表注單資料
            page_data (dict): 後台頁面統計資料

        Returns:
            (dict|bool): 回傳確認資料，異常則是False
        """

        try:
            if not bet_list:
                return False

            # init
            result_list = []
            manage_check_data = {}
            field_dict = {}
            total_bet_amount = 0
            total_bet_win_lost = 0
            total_true_bet = 0

            # 資料欄位
            th_list = self.driver.find_elements(By.XPATH, "//thead//th")
            for index, ele in enumerate(th_list):
                # +1 是給Xpath用
                field_dict[ele.text] = index + 1

            # 取得會員報表頁面各總數
            ele = self.driver.find_elements(By.XPATH, "//tbody/tr")
            total_number = len(ele)
            for i in field_dict:
                if i == '下注金额':
                    ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                        (By.XPATH, f"//tfoot/tr[2]/td[{field_dict[i]}]")))
                    total_bet_amount = float(decimal.Decimal(ele.text.replace(',', '')))
                elif i == '会员结果':
                    ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                        (By.XPATH, f"//tfoot/tr[2]/td[{field_dict[i]}]")))
                    total_bet_win_lost = decimal.Decimal(ele.text.replace(',', ''))
                    total_bet_win_lost = float(str(total_bet_win_lost)[:-1])
                elif i == '实投量':
                    ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                        (By.XPATH, f"//tfoot/tr[2]/td[{field_dict[i]}]")))
                    total_true_bet = float(decimal.Decimal(ele.text.replace(',', '')))

            # 統計比對數字
            # 報表總筆數
            report_total_number = len(bet_list)

            # 報表總下注金額(計算後在轉float避免顯示有誤)
            report_total_bet_amount = 0
            for i in bet_list:
                report_total_bet_amount += decimal.Decimal(i['下注金额'])
            report_total_bet_amount = float(report_total_bet_amount)

            # 報表總會員結果(計算後在轉float避免顯示有誤)
            report_bet_win_lost = 0
            for i in bet_list:
                report_bet_win_lost += decimal.Decimal(i['会员结果'])
            report_bet_win_lost = float(report_bet_win_lost)

            # 報表總實投量(計算後在轉float避免顯示有誤)
            report_total_actual_bet_amount = 0
            for i in bet_list:
                report_total_actual_bet_amount += decimal.Decimal(i['实投量'])
            report_total_actual_bet_amount = number_check(float(report_total_actual_bet_amount))

            if page_data:
                print("檢查項目：「會員報表頁面下方總資料」與「代理商搜尋結果」比對")

                # 比對總筆數
                if total_number != page_data['笔数']:
                    print("Fail：會員報表下方「總筆數」與代理商搜尋結果「總筆數」不同")
                    print(f"會員報表「總筆數」：{total_number}")
                    print(f"代理商搜尋結果「總筆數」：{page_data['笔数']}\n")
                    result_list.append("False")

                # 比對總下注金額
                if total_bet_amount != page_data['下注金额']:
                    print("Fail：會員報表「總下注金額」與代理商搜尋結果「總下注金額」不同")
                    print(f"會員報表「總下注金額」：{total_bet_amount}")
                    print(f"代理商搜尋結果「總下注金額」：{page_data['下注金额']}\n")
                    result_list.append("False")

                # 比對總下注結果(外層報表資料只顯示到小數點一位數，故調整內層報表資料小數位顯示)
                if total_bet_win_lost != page_data['会员结果']:
                    print("Fail：會員報表「會員結果」與代理商搜尋結果「會員結果」不同")
                    print(f"會員報表「會員結果」：{total_bet_win_lost}")
                    print(f"代理商搜尋結果「會員結果」：{page_data['会员结果']}\n")
                    result_list.append("False")

                # 比對總實投量
                if total_true_bet != page_data['实投量']:
                    print("Fail：後台「總實投量」與代理商搜尋結果「總實投量」不同")
                    print(f"會員報表「總實投量」：{total_true_bet}")
                    print(f"代理商搜尋結果「總實投量」：{page_data['实投量']}\n")
                    result_list.append("False")

                print("*** 檢查結束 ***\n")
            else:
                manage_check_data['總筆數'] = report_total_number
                manage_check_data['總下注金額'] = report_total_bet_amount
                manage_check_data['總下注結果'] = report_bet_win_lost
                manage_check_data['總實投量'] = report_total_actual_bet_amount

            print("檢查項目：「會員報表第一層」與「會員報表第二層」比對")
            for bet in bet_list:
                number_key = list(bet['内容'].keys())[0]

                # 確認第一層與第二層金額、輸贏是否相同
                first_bet = decimal.Decimal(bet['下注金额'])
                first_win_lost = decimal.Decimal(bet['会员输赢'])
                second_bet = 0
                second_win_lost = 0

                # 計算第二層下注金額、輸贏金額
                for bet_second in bet['内容'][number_key]:
                    second_bet += decimal.Decimal(bet_second['下注金额'])
                    second_win_lost += decimal.Decimal(bet_second['输赢金额'])

                # 計算後在轉float避免顯示有誤
                first_bet = float(first_bet)
                first_win_lost = float(first_win_lost)
                second_bet = float(second_bet)
                second_win_lost = float(second_win_lost)

                # 比對下注金額
                if first_bet != second_bet:
                    print("Fail：第一層「下注金額」與第二層不同")
                    print(f"編號：{bet['编号']}")
                    print(f"第一層「下注金額」：{first_bet}")
                    print(f"第二層「下注金額」：{second_bet}\n")
                    result_list.append("False")

                # 比對輸贏金額(因實際後台小數位問題，允許誤差值1以內)
                elif (first_win_lost - second_win_lost) > 1:
                    print("Fail：第一層「輸贏金額」與第二層不同")
                    print(f"編號：{bet['编号']}")
                    print(f"第一層「輸贏金額」：{first_win_lost}")
                    print(f"第二層「輸贏金額」：{second_win_lost}\n")
                    result_list.append("False")
            print("*** 檢查結束 ***\n")
            print("==================== 後台報表 - 檢查結束 ====================\n")

            manage_check_data['確認結果'] = False if "False" in str(result_list) else True

            return manage_check_data

        except Exception as e:
            self.get_img("Fail：確認後台統計資料與報表資料異常")
            print(e)
            return False
