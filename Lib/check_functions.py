# _*_ coding: UTF-8 _*_
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time


class CheckFunctions:
    """ 確認函式 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def check_all_round(self, all_round):
        """
        確認所有下注注區是否為 True
        參數:
            all_round (dict): 遊戲注區

        Returns:
            bool: 判斷全部成功回傳 True，失敗則是 False
        """

        result_list = [all_round[i] for i in all_round]
        return False if "False" in str(result_list) else True

    def check_bet_success(self, game_round, all_game_round, fantan=False):
        """
        確認下注是否成功
        參數:
            game_round (dict): 注區
            all_game_round (dict): 所有注區

        Returns:
            bool: 下注成功，修改注區為True，回傳所有注區 ，失敗則是 False
        """

        try:
            # 點擊前等待1秒，避免過快造成錯誤
            time.sleep(1)

            if fantan:
                # 點擊確定
                WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[text()='确定']"))).click()

                # 判斷下注成功提示是否有出現
                WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//span[text()='下注成功']")))
                self.get_img("下注成功")
            else:
                # 點擊確定
                WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[text()=' 确定 ']"))).click()

                # 判斷下注成功提示是否有出現
                WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@data-text='下注成功']")))
                self.get_img("下注成功")

            all_game_round[game_round] = True

            return all_game_round

        except:
            self.get_img("下注失敗")
            return False
