import time
import datetime
import requests
import decimal
from Setting import *


class CheckReportApi:
    """ 確認報表API """

    @staticmethod
    def format_report(resp_data, game_name):
        """
        整理注單報表
        參數:
            resp_data (dict): 注單資料
            game_name (str): 遊戲名稱

        Returns:
            list: 回傳整理後的注單報表
        """

        bet_list = []
        for bet_data in resp_data['data']:
            bet_dict = {}
            for i in bet_data:
                # 因賠率包含本金，故需要減 1
                if i == "Odds":
                    # 計算時會有誤差，故使用decimal
                    bet_dict[i] = float(decimal.Decimal(bet_data[i]) - 1)

                # 派彩、下注金額，轉float
                elif i == "WinLosePoint" or i == "BetPoint":
                    bet_dict[i] = float(bet_data[i])

                # 注單編號、局號、遊戲名稱、下注區域ID、下注區域名稱
                elif i in ["RecordId", "NoActive", "GameName", "AreaId", "AreaName"]:
                    bet_dict[i] = bet_data[i]

            # 有帶入遊戲名稱時，只加入遊戲名稱相同的資料
            if game_name:
                if bet_data['GameName'] == game_name:
                    bet_list.append(bet_dict)
                else:
                    continue
            else:
                # 回傳全部的資料
                bet_list.append(bet_dict)

        return bet_list

    def get_report(self, date='', start_time='', end_time='', game_name=''):
        """
        取得注單報表
        參數:
            date (str): 查詢日期 (YYYY-MM-DD)，預設為空
            start_time (str): 開始時間 (hh:mm:ss)，預設為空
            end_time (str): 結束時間 (hh:mm:ss)，預設為空
            game_name (str): 遊戲名稱 ，預設為空

        Returns:
            list | False: 查詢報表成功，回傳注單報表，失敗則是 False，若有帶入 game_name 且無注單則是 False
        """

        url = test_data['api_url']

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }

        # 時間處理
        if date:
            search_date = date
        else:
            # 取得當天日期 (美東)
            tz_utc_4 = datetime.timezone(datetime.timedelta(hours=-4))
            search_date = datetime.datetime.today().astimezone(tz_utc_4).strftime("%Y-%m-%d")

        if start_time:
            start_date = f'{search_date} {start_time}'
        else:
            # 預設為 12:00:00
            start_date = f'{search_date} 12:00:00'

        if end_time:
            end_date = f'{search_date} {end_time}'
        else:
            # 預設為 11:59:59
            old_date = datetime.datetime.strptime(search_date + ' 11:59:59', '%Y-%m-%d %H:%M:%S')
            # 預設查詢一整天，因美東時區，需要加一天
            end_date = (old_date + datetime.timedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S')

        # 時間區間請勿超過一天
        data = {
            "groupId": test_data['groupId'],  # QA 測試專用
            "memberAccount": test_data['member_account'][0],  # 會員帳號
            "startTime": start_date,  # 開始時間
            "endTime": end_date,  # 結束時間
        }

        response = requests.post(url=url, json=data, headers=headers)
        resp_data = response.json()

        if response.status_code != 200:
            print("Fail：API 異常")
            print(resp_data)
            print(f"帳號：{data['memberAccount']}")
            print(f"開始時間：{data['startTime']}")
            print(f"結束時間：{data['endTime']}")
            return False

        else:
            if "data" not in resp_data:
                print("Fail：API 異常")
                print(resp_data)
                print(f"帳號：{data['memberAccount']}")
                print(f"開始時間：{data['startTime']}")
                print(f"結束時間：{data['endTime']}")
                return False

            elif len(resp_data['data']) == 0:
                print("Fail：該時段無注單")
                print(f"帳號：{data['memberAccount']}")
                print(f"開始時間：{data['startTime']}")
                print(f"結束時間：{data['endTime']}")
                return False

            else:
                # 資料處裡
                report_list = self.format_report(resp_data, game_name)
                # 判斷有無注單
                if game_name and len(report_list) == 0:
                    print(f"Fail：該時段無 {game_name} 注單資料")
                    print(f"帳號：{data['memberAccount']}")
                    print(f"開始時間：{data['startTime']}")
                    print(f"結束時間：{data['endTime']}")
                    return False

        return report_list

    @staticmethod
    def check_all_odds(all_bet_data, odds_list):
        """
        確認注單所有賠率
        參數:
            all_bet_data (dict): 所有注單資料
            odds_list (list): 賠率表

        Returns:
            True | False: 賠率ID有在賠率表中為成功，不存在則是 False
        """

        result_list = []

        for odds_dict in odds_list:
            odds_id, _ = odds_dict.keys()
            check_result = []

            for i in all_bet_data:
                # 賠率有在賠率表中
                if odds_id in i.values():
                    check_result.append("True")

            result = True if "True" in str(check_result) else False
            if not result:
                print(f"Fail：賠率ID：{odds_id}、注區名稱：{odds_dict['AreaName']} 無下注資料\n")
                result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results

    @staticmethod
    def check_odds(bet_data, odds_list):
        """
        確認注單賠率
        參數:
            bet_data (dict): 注單資料
            odds_list (list): 賠率表

        Returns:
            True | False: 賠率相同成功，不同則是 False
        """

        for odds_dict in odds_list:
            odds_id, _ = odds_dict.keys()

            if bet_data['AreaId'] == odds_id:
                if bet_data['Odds'] != float(odds_dict[odds_id]):
                    print("\nFail：注單賠率錯誤")
                    print(f"注單編號：{bet_data['RecordId']}")
                    print(f"賠率ID：{bet_data['AreaId']}")
                    print(f"賠率值：{bet_data['Odds']}")
                    print(f"正確賠率表：{float(odds_dict[odds_id])}")
                    print(f"注區名稱：{odds_dict['AreaName']}")
                    return False
                else:
                    return True

    @staticmethod
    def check_win_lose(bet_data):
        """
        確認派彩正負
        參數:
            bet_data (dict): 注單資料

        Returns:
            True | False: 派彩為正成功，為負則是 False
        """

        if bet_data['WinLosePoint'] < 0:
            print("\nFail：注單派彩為負")
            print(f"注單編號：{bet_data['RecordId']}")
            print(f"賠率ID：{bet_data['AreaId']}")
            print(f"派彩：{bet_data['WinLosePoint']}")
            return False
        else:
            return True

    @staticmethod
    def check_pay_amount(bet_data):
        """
        確認派彩計算
        參數:
            bet_data (dict): 注單資料

        Returns:
            True | False: 派彩計算與注單派彩相同為成功，不同則是 False
        """

        # 計算派彩
        pay_amount = bet_data['BetPoint'] * bet_data['Odds']

        if pay_amount != bet_data['WinLosePoint']:
            print("\nFail：注單派彩計算有誤")
            print(f"注單編號：{bet_data['RecordId']}")
            print(f"賠率ID：{bet_data['AreaId']}")
            print(f"賠率值：{bet_data['Odds']}")
            print(f"下注金額：{bet_data['BetPoint']}")
            print(f"派彩：{bet_data['WinLosePoint']}")
            print(f"計算後派彩：{pay_amount}")
            return False
        else:
            return True
