# _*_ coding: UTF-8 _*_
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from Lib import get_report


class ReportComparison:
    """ 亞洲商務 - 報表比對 """

    def __init__(self, driver, get_img, window_dict):
        self.driver = driver
        self.get_img = get_img
        self.window_dict = window_dict

    def auto_report(self, now=True):
        """
        取得會員端報表資料
        :return:
        """
        # init
        result_list = []

        # 切換至官網
        self.driver.switch_to.window(self.window_dict['member_window'])
        self.driver.refresh()

        # wait loading
        WebDriverWait(self.driver, 10).until_not(EC.presence_of_element_located(
            (By.XPATH, "//div[@class='loading-container']")))

        try:
            # 點擊帳戶歷史
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@data-i18n='accounthistory']"))).click()
            time.sleep(1)

            # wait loading
            WebDriverWait(self.driver, 10).until_not(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='loading-container']")))
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[text()='投注记录']")))

            if not now:
                # 查詢昨日帳
                WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[text()='昨日']"))).click()
                WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[text()='查询']"))).click()

                # wait loading
                WebDriverWait(self.driver, 10).until_not(EC.presence_of_element_located(
                    (By.XPATH, "//div[@class='loading-container']")))

        except:
            self.get_img("Fail：進入客端報表失敗\n")
            return False

        # 確認有無資料
        try:
            WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(
                (By.XPATH, "//div[text()='没有要显示的记录']")))
            self.get_img("Fail：客端投注紀錄無資料\n")
            return False
        except:
            pass

        # 取得官網報表資料
        get_member_report = get_report.MemberReport(self.driver, self.get_img)
        member_report = get_member_report.format_report()
        if not member_report:
            result_list.append("False")
        else:
            # 確認資料
            check_data = get_member_report.check_web_and_report_data(member_report)
            if not check_data['確認結果']:
                result_list.append("False")

        # 切換至後台
        self.driver.switch_to.window(self.window_dict['manage_window'])
        self.driver.refresh()

        try:
            # 進入報表
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[text()='报表']"))).click()
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//button[text()=' 输赢报表 ']"))).click()

            if not now:
                # 查詢昨日帳
                WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//button[text()=' 昨日 ']"))).click()

            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//button[text()=' 查询 ']"))).click()
            time.sleep(2)
        except:
            self.get_img("Fail：進入後台報表失敗\n")
            return False

        # 確認有無資料
        try:
            WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(
                (By.XPATH, "//td[text()='没有匹配结果']")))
            self.get_img("Fail：後台報表無資料\n")
            return False
        except:
            pass

        # 取得後台報表資料
        get_manage_report = get_report.ManageReport(self.driver, self.get_img)
        manager_report, page_data, page_check_result = get_manage_report.search_report(check_data)
        if not manager_report or not page_check_result:
            result_list.append("False")
        else:
            # 確認資料
            manage_check_data = get_manage_report.check_web_and_report_data(manager_report, page_data)
            if not manage_check_data['確認結果']:
                result_list.append("False")

            # 比對官網與後台報表
            check_report_result = get_report.check_member_report_and_manager_report(member_report, manager_report)
            if not check_report_result:
                result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results
