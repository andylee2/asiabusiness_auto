import sys
import os
import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from HTMLTestRunner import HTMLTestRunner
from datetime import datetime
from Setting import *
import report_comparison
from Games import Baccarat, DragonTiger, FanTan

sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "..")))
options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-automation'])
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
get_img = HTMLTestRunner().get_img
driver.maximize_window()

# 視窗控制
window_dict = {
    'member_window': driver.current_window_handle,
    'manage_window': None,
}


class AutoAsiaBusiness(unittest.TestCase):
    """ 亞洲商務自動化測試 """

    report_compare = report_comparison.ReportComparison(driver, get_img, window_dict)  # 報表比對(當前、歷史)
    baccarat_ab = Baccarat.BaccaratAutoBet(driver, get_img)  # 百家樂
    dragontiger_ab = DragonTiger.DragonTigerAutoBet(driver, get_img)  # 龍虎
    fantan_ab = FanTan.FanTanAutoBet(driver, get_img)  # 番攤

    @classmethod
    def setUpClass(cls) -> None:
        # 測試是否繼續變數
        cls.test_continue = True

    def setUp(self) -> None:
        """ 每個測試項目測試之前調用 """

        # 判斷測試是否繼續
        if not self.__class__.test_continue:
            self.skipTest("中斷測試")

    def test_login(self):
        """ 會員端登入 """

        # init
        result_list = []
        driver.get(test_data['member_url'])
        time.sleep(2)

        try:
            get_img("會員端登入頁面")

            # 輸入帳號密碼
            input_acc = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@id='username']")))
            input_acc.send_keys(test_data['member_account'][0])

            input_pwd = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@id='password']")))
            input_pwd.send_keys(test_data['member_account'][1])

            # 獲取驗證碼 -> 輸入驗證碼
            verify_capcha = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='capcha']"))).text
            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@id='verifyCapcha']"))).send_keys(verify_capcha)

            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@data-i18n='login.login']"))).click()

            # 確認是否登入成功
            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@data-i18n='accounthistory']")))
            window_dict['member_window'] = driver.current_window_handle
            get_img("登入成功")
        except:
            get_img("登入失敗")
            result_list.append("False")
            self.__class__.test_continue = False

        results = False if "False" in str(result_list) else True
        self.assertEqual(True, results)

    def test_manage_login(self):
        """ 後台登入 """

        # init
        result_list = []

        # 開新網址
        driver.execute_script(f"window.open('{test_data['agent_url']}');")
        handles = driver.window_handles
        time.sleep(1)
        driver.switch_to.window(handles[-1])

        try:
            get_img("後台登入頁面")

            # 輸入帳號密碼
            input_acc = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@id='txtUserName']")))
            input_acc.send_keys(test_data['agent_account'][0])

            input_pwd = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@id='txtPassword']")))
            input_pwd.send_keys(test_data['agent_account'][1])

            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='login']"))).click()

            # 確認是否登入成功
            check_name = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//span[@class='username']"))).text

            if check_name == test_data['agent_account'][0]:
                window_dict['manage_window'] = driver.current_window_handle
                get_img("登入成功")
            else:
                get_img(f"登入失敗，登入帳號:{test_data['agent_account'][0]}，顯示帳號為:{check_name}")
                result_list.append("False")
                self.__class__.test_continue = False
        except Exception as e:
            print(e)
            get_img("登入失敗")
            result_list.append("False")
            self.__class__.test_continue = False

        results = False if "False" in str(result_list) else True
        self.assertEqual(True, results)

    def test_report_now(self):
        """ 亞洲商務報表比對 - 當前 """

        result = self.report_compare.auto_report()
        self.assertEqual(True, result)

    def test_report_last(self):
        """ 亞洲商務報表比對 - 歷史 """

        result = self.report_compare.auto_report(now=False)
        self.assertEqual(True, result)

    def test_baccarat(self):
        """ 百家樂自動下注 """

        result = self.baccarat_ab.auto_bet()
        self.assertEqual(True, result)

    def test_dragontiger(self):
        """ 龍虎自動下注 """

        result = self.dragontiger_ab.auto_bet()
        self.assertEqual(True, result)

    def test_fantan(self):
        """ 番攤自動下注 """

        result = self.fantan_ab.auto_bet()
        self.assertEqual(True, result)

if __name__ == '__main__':
    test_units = unittest.TestSuite()

    # 報表比對
    # test_units.addTests([
    #     AutoAsiaBusiness("test_login"),
    #     AutoAsiaBusiness("test_manage_login"),
    #     AutoAsiaBusiness("test_report_now"),
    #     AutoAsiaBusiness("test_report_last")
    # ])

    # 自動下注
    test_units.addTests([
        AutoAsiaBusiness("test_login"),
        AutoAsiaBusiness("test_baccarat"),
        AutoAsiaBusiness("test_dragontiger"),
        AutoAsiaBusiness("test_fantan")
    ])

    now = datetime.now().strftime('%m-%d %H_%M_%S')
    filename = './Report/' + now + '.html'
    with open(filename, 'wb+') as fp:
        runner = HTMLTestRunner(
            stream=fp,
            verbosity=2,
            title=f'亞洲商務自動化 - 測試環境:UAT',
            driver=driver
        )
        runner.run(test_units)

    driver.quit()