# _*_ coding: UTF-8 _*_

test_data = {
    "agent_url": "https://ag.acsd888.com/",     # 後台網址
    "member_url": "https://abg1.acsd888.com/",  # 會員端網址
    "agent_account": ["Aut0004", "6666"],      # 代理商帳號
    "member_account": ["JAutotest0", "6666"],    # 會員帳號
    "api_url": "https://tomandjerry.dev-royalgame.com/GetBetRecord",
    "groupId": "4"
}